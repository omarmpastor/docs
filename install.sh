#!/bin/bash

sudo pacman -Syu --noconfirm

sh ./bspwm/install.sh
sh ./bspwm/config.sh

sudo mkdir /usr/share/wallpapers
sudo cp ./common/images/Arch-Wallpaper-1080-*.png /usr/share/wallpapers/

echo
echo
echo "Reboot system or execute 'sudo systemctl start lightdm'"
echo
echo
