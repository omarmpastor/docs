#!/bin/bash

#
# Video
#
sudo pacman -S nvidia nvidia-settings nvidia-utils libva-utils vdpauinfo libvdpau

sudo pacman -S opencl-nvidia ffnvcodec-headers libxnvctrl clinfo ocl-icd opencl-headers
sudo pacman -S cuda cudnn

#
# Sonido
#


sudo pacman -S alsa-utils alsa-plugins alsa-oss libsamplerate
sudo modprobe snd_seq_oss snd_pcm_oss snd_mixer_oss

# Para controlar el sonido por consola ejecutamos: `alsamixer`

# En Virtualbox ejecutar `sudo alsactl init`


# Remuestreo de alta calidad
# En algunos sistemas, samplerate_best puede causar un problema por el que no se obtiene ningún sonido desde flashplayer.
sudo echo 'defaults.pcm.rate_converter "samplerate_best"' > /etc/asound.conf

# Si queremos forzar que una tarjeta se cargue primero
#echo "options snd_hda_intel index=1" >> /etc/modprobe.d/alsa-base.conf


# Para usar pulseaudio en vez de alsa
# pacman -S pulseaudio pulseaudio-bluetooth pulseaudio-alsa
# pactl load-module module-switch-on-connect
