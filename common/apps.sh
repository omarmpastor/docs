#!/bin/bash

#
# browsers
#
sudo sudo pacman -S chromium firefox --noconfirm

#
# tools
#
sudo sudo pacman -S thunar vlc code neovim zsh bat lsd ranger kitty unzip unrar ntfs-3g xautolock --noconfirm

# Añadimos alias a ~/.zshrc o a ~/.bashrc (el que usemos)
cat >> ~/.bashrc <<- EOM
# Alias
alias ll='lsd -lh'
EOM

cat > ~/.zshrc <<- EOM
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
# End of lines configured by zsh-newuser-install
EOM



#
# other tools
#
yay -S sublime-text-4 --noconfirm
yay -S betterlockscreen --noconfirm
# yay -S lf --noconfirm (usamos ranger a ver...)
