#!/bin/bash

mkdir -p ~/.config/picom

#
# config
#
cp /etc/xdg/picom.conf ~/.config/picom/

sed -i -E "s/^(shadow-exclude = \[)$/\1\n  \"class_g = 'Rofi'\",/" ~/.config/picom/picom.conf
sed -i -E "s/^(inactive-opacity = )[0-9\.]*;?$/\10.85;/" ~/.config/picom/picom.conf
sed -i -E "s/^# (active-opacity = )[0-9\.]*;?$/\10.95;/" ~/.config/picom/picom.conf
sed -i -E "s/^(fade-in-step = )[0-9\.]*;?$/\10.5/" ~/.config/picom/picom.conf
sed -i -E "s/^(fade-out-step = )[0-9\.]*;?$/\10.5/" ~/.config/picom/picom.conf
sed -i -E "s/^(focus-exclude = \[).*(\];?)$/\1\n  \"class_g = 'Chromium'\",\n  \"class_g = 'Firefox'\",\n  \"class_g = 'code-oss'\",\n  \"class_g = 'vlc'\",\n  \"class_g = 'Thunar'\" \n\2/" ~/.config/picom/picom.conf
sed -i -E "s/^(rounded-corners-exclude = \[)$/\1\n  \"class_g = 'Rofi'\",/" ~/.config/picom/picom.conf
sed -i -E "s/^(mark-ovredir-focused = )true;?$/\1false;/" ~/.config/picom/picom.conf

sed -i -E "s/^# (opacity-rule = \[)\];?$/\1\n\
  \"100:class_g = 'Chromium'\",\n \
 \"100:class_g = 'Firefox'\",\n \
 \"100:class_g = 'code-oss'\",\n \
 \"100:class_g = 'vlc'\",\n \
 \"100:class_g = 'Rofi'\",\n \
 \"100:class_g = 'Thunar'\",\n \
 \"100:class_g = 'Subl'\"\n \
]; \
/" ~/.config/picom/picom.conf

echo "picom -b" >> ~/.config/bspwm/bspwmrc


# Esto hace que se dispare el consumo de memoria asi que no lo ponemos
# sed -i -E "s/^(corner-radius = )[0-9]*;?$/\16/" ~/.config/picom/picom.conf
# sed -i -E "s/^(shadow-radius = )[0-9]*;?$/\16/" ~/.config/picom/picom.conf
