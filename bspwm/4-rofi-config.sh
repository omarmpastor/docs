#!/bin/bash

mkdir -p ~/.config/rofi

#
# config
#
cat > ~/.config/rofi/config.rasi <<- EOM
configuration {
  modes: "drun,combi";
  font: "HackNF";
  show-icons: true;
  combi {
      modi: "window,drun,ssh";
  }
}
@theme "Arc-Dark"
EOM

sed -i 's/dmenu_run/rofi -show/' ~/.config/sxhkd/sxhkdrc
