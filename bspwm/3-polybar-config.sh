#!/bin/bash

mkdir ~/.config/polybar
cp /usr/share/doc/polybar/examples/config.ini ~/.config/polybar/config.ini


#
# config
#
cat > ~/.config/polybar/launch.sh <<- EOM
#!/bin/bash

killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

polybar example &
EOM

chmod +x ~/.config/polybar/launch.sh
echo "$HOME/.config/polybar/launch.sh" >> ~/.config/bspwm/bspwmrc

#
# config
#
sed -i 's/^font-0 = [ A-Za-z0-9;]*$/font-0 = Hack Nerd Font:size=10;2/' ~/.config/polybar/config.ini
sed -i 's/^modules-left = [ A-Za-z0-9]*$/modules-left = xworkspaces xwindow/' ~/.config/polybar/config.ini
sed -i 's/^modules-right = [ A-Za-z0-9]*$/modules-right = alsa cpu memory wlan date/' ~/.config/polybar/config.ini
sed -i '/label-[a-z]*-padding = 2/ s/= 2/= 1/' ~/.config/polybar/config.ini
sed -i -E 's/^(label-connected = ).*$/\1%ifname% %downspeed%/' ~/.config/polybar/config.ini
sed -i -E 's/^(format-prefix = )"RAM "*/\1" "/' ~/.config/polybar/config.ini
sed -i -E 's/^(format-prefix = )"CPU "*/\1" "/' ~/.config/polybar/config.ini


cat >> ~/.config/polybar/config.ini <<- EOM
[module/alsa]
type = internal/alsa
master-soundcard = default
speaker-soundcard = default
headphone-soundcard = default
master-mixer = Master
interval = 5

format-volume = <ramp-volume> <label-volume>
label-muted = 󰝟 0%
label-muted-foreground = #66

ramp-volume-0 = 󰕿
ramp-volume-1 = 󰖀
ramp-volume-2 = 󰕾

ramp-headphones-0 = 󰟎
ramp-headphones-1 = 󰋋
EOM


# label-connected = %essid% %downspeed:9%
# label-connected = %ifname% %downspeed%
# %netspeed% -> %upspeed% + %downspeed%
# %signal% only wireless
