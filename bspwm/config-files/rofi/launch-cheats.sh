#/bin/bash/

FILE_CONFIG="config-cheats.rasi"
SCRIPTPATH=$(dirname "$0")

if [ ! $0 ]; then
    SCRIPTPATH=$(dirname .)
fi

if [ ! -f ${SCRIPTPATH}/${FILE_CONFIG} ]; then
    echo "File config not found!"
    exit 1 
fi

DOC=$(ls ${SCRIPTPATH}/cheats | rofi -theme ${SCRIPTPATH}/${FILE_CONFIG} -dmenu -p filter)

if [ ! -f ${SCRIPTPATH}/cheats/${DOC} ]; then
    echo "Help not found!"
    exit 2
fi

cat ${SCRIPTPATH}/cheats/${DOC} | rofi -theme ${SCRIPTPATH}/${FILE_CONFIG} -dmenu -p filter
