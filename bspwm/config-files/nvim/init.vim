" Tabs
set tabstop=2
set shiftwidth=2
set expandtab

" Clipboard
set clipboard+=unnamedplus
