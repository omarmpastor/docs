#!/bin/bash

SCRIPTPATH=$(dirname "$0")

sh ${SCRIPTPATH}/1-bspwm-config.sh
sh ${SCRIPTPATH}/2-picom-config.sh
sh ${SCRIPTPATH}/3-polybar-config.sh
sh ${SCRIPTPATH}/4-rofi-config.sh
sh ${SCRIPTPATH}/5-scrot-config.sh
