#!/bin/bash

#
# config
#

mkdir ${HOME}/Images

cat >> ~/.config/sxhkd/sxhkdrc <<- EOM
@Print
    scrot -ozs -F ${HOME}/Images/%Y%m%dT%H%M%S.png

alt + @Print
    scrot -ozd 5 ${HOME}/Images/%Y%m%dT%H%M%S.png

ctrl + @Print
    scrot -ozu ${HOME}/Images/%Y%m%dT%H%M%S.png
EOM

