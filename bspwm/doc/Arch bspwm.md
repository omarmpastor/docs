# Instalar bspwm

Instalamos bspwm, el menu, xorg y la terminal (rxvt-unicode tiene un bug en 2023 que mete 32 lineas en blanco al abrir)
```sh
sudo pacman -S bspwm sxhkd xorg-server xorg-xinit xorg-xrandr feh dmenu rxvt-unicode xterm
# Podemos elegir otro menu y otra terminal y cambirlo en ~/.config/sxhkd/sxhkdrc, mas adelante configuramos xterm y rofi
```

Otras herramientas que vamos a necesitar mas adelante
```sh
sudo pacman -S git curl wget unzip vim
#sudo pacman -S 
```


Copiamos la configuracion a nuestro perfil
```sh
mkdir -p ~/.config/{bspwm,sxhkd}
cp /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/
cp /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/
#chmod u+x ~/.config/bspwm/bspwmrc

sed -i 's/urxvt/xterm/' ~/.config/sxhkd/sxhkdrc # Cambiamos la terminal ya que urxvt tiene un bug
```

Cambiamos la configuracion de bspwm
```sh
sed -i 's/^bspc monitor -d .*$/bspc monitor -d   󰡨 󰷉 /' ~/.config/bspwm/bspwmrc
sed -i 's/^bspc config border_width.*$/bspc config border_width         1/' ~/.config/bspwm/bspwmrc
sed -i 's/^bspc config window_gap.*$/bspc config window_gap           6/' ~/.config/bspwm/bspwmrc
sed -i 's/^bspc rule -a.*//' ~/.config/bspwm/bspwmrc
echo "bspc config focused_border_color '#4C0099'" >> ~/.config/bspwm/bspwmrc
```

Ponemos que se arranque al inicio
```sh
# Comprobamos que en ~/.config/bspwm/bspwmrc está la linea 'sxhkd &', sino la ponemos
echo "exec bspwm" > ~/.xsession
```

Configuramos la variable
```sh
echo 'export XDG_CONFIG_HOME=$HOME/.config' >> ~/.bashrc
```

Ponemos el teclado en español ejecutando
```sh
cat > /tmp/00-keyboard.conf <<- EOM
Section "InputClass"
        Identifier "system-keyboard"
        MatchIsKeyboard "on"
        Option "XkbLayout" "es"
EndSection
EOM

sudo mv /tmp/00-keyboard.conf /etc/X11/xorg.conf.d/
```

## Instalamos gestor de sesiones lightdm (podemos elegir otro, por ejemplo sddm)

```sh
sudo pacman -S lightdm lightdm-gtk-greeter
```

Descomentamos `greeter-session` que esta mas abajo de la linea `[Seat:*]`, no mas arriba, en el archivo `/etc/lightdm/lightdm.conf` y dejamos la linea:
```sh
greeter-session=lightdm-gtk-greeter
```

Activamos el servicio
```sh
sudo systemctl enable lightdm.service
```

## Personalizar colores y configuración

Creamos el archivo ~/.Xresources
```sh
!! ColorScheme

! Generated with :
! XRDB2Xreources.py
!
*.foreground:  #e5e5e5
*.background:  #232322
*.cursorColor: #16afca
!
! Black
*.color0:      #212121
*.color8:      #424242
!
! Red
*.color1:      #b7141f
*.color9:      #e83b3f
!
! Green
*.color2:      #457b24
*.color10:     #7aba3a
!
! Yellow
*.color3:      #f6981e
*.color11:     #ffea2e
!
! Blue
*.color4:      #134eb2
*.color12:     #54a4f3
!
! Magenta
*.color5:      #560088
*.color13:     #aa4dbc
!
! Cyan
*.color6:      #0e717c
*.color14:     #26bbd1
!
! White
*.color7:      #efefef
*.color15:     #d9d9d9
!
! Bold, Italic, Underline
*.colorBD:     #b7141f
!*.colorIT:
!*.colorUL:

!! URxvt Appearance

URxvt.font: 			xft:Hack Nerd Font Mono:pixelsize=12:antialias=false
URxvt.boldFont: 		xft:Hack Nerd Font Mono:bold:pixelsize=12:antialias=false
!! URxvt.letterSpace:		-1

!! URxvt Config

URxvt.scrollBar:      		      false

!! xterm Appearance

xterm*faceName: xft:Hack Nerd Font Mono
xterm*faceSize: 10
xterm*loginshell: false
```

## Personalizar bash


Cambiar prompt bash
```sh
sed -i "s/^PS1/#PS1/" ~/.bashrc
echo "PS1='\[\e[1;34m\]→ \[\e[0;34m\]\w \[\e[0m\]'" >> ~/.bashrc

# Y el de root. Si existe .bashrc, comentamos la linea PS1=xxx
echo "PS1='\[\e[1;31m\]# \[\e[0;31m\]\w \[\e[0m\]'" >> /root/.bashrc
```

## Fuentes

Instalamos las fuentes
```sh
# Podemos ver la ultima version en https://github.com/ryanoasis/nerd-fonts -> pinchamos en Release y copiamos el enlace
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.1/Hack.zip
sudo unzip Hack.zip -d /usr/share/fonts/
rm Hack.zip
```

#### Si queremos instalar todas las fuentes nerd

```sh
git clone --depth=1 https://github.com/romkatv/nerd-fonts.git
cd nerd-fonts
./install.sh
cd .. && rm -rf nerd-fonts
# Esto almacena las fuentes en $HOME/.local/share/fonts, las del sistema estan en /usr/share/fonts/
```

## Instalar transparencias

```sh
sudo pacman -S picom
mkdir -p ~/.config/picom
```

Copiamos la configuracion y la modificamos
```sh
cp /etc/xdg/picom.conf ~/.config/picom/

sed -i -E "s/^(shadow-exclude = \[)$/\1\n  \"class_g = 'Rofi'\",/" ~/.config/picom/picom.conf
sed -i -E "s/^(inactive-opacity = )[0-9\.]*;?$/\10.85;/" ~/.config/picom/picom.conf
sed -i -E "s/^# (active-opacity = )[0-9\.]*;?$/\10.95;/" ~/.config/picom/picom.conf
sed -i -E "s/^(fade-in-step = )[0-9\.]*;?$/\10.5/" ~/.config/picom/picom.conf
sed -i -E "s/^(fade-out-step = )[0-9\.]*;?$/\10.5/" ~/.config/picom/picom.conf
sed -i -E "s/^(focus-exclude = \[).*(\];?)$/\1\n  \"class_g = 'Chromium'\",\n  \"class_g = 'Firefox'\",\n  \"class_g = 'code-oss'\",\n  \"class_g = 'vlc'\",\n  \"class_g = 'Thunar'\" \n\2/" ~/.config/picom/picom.conf
sed -i -E "s/^(rounded-corners-exclude = \[)$/\1\n  \"class_g = 'Rofi'\",/" ~/.config/picom/picom.conf
sed -i -E "s/^(corner-radius = )[0-9]*;?$/\16/" ~/.config/picom/picom.conf
sed -i -E "s/^(shadow-radius = )[0-9]*;?$/\16/" ~/.config/picom/picom.conf
sed -i -E "s/^(mark-ovredir-focused = )true;?$/\1false;/" ~/.config/picom/picom.conf
```

Ponemos que se arranque al inicio
```sh
echo 'picom -b' >> ~/.config/bspwm/bspwmrc
```

Si queremos saber la clase de algun programa, ejecutamos (paquete xorg-xprop) el comando (le pinchamos y nos dice el nombre, es el ultimo de los que muestra):
```sh
xprop WM_CLASS
```

Lo ponemos en ~/.config/picom/picom.conf
```sh
    "class_g = 'APP'"
```

## Instalar polybar

```sh
sudo pacman -S polybar
```

```sh
mkdir ~/.config/polybar
cp /usr/share/doc/polybar/examples/config.ini ~/.config/polybar/config.ini
```

Creamos el archivo ~/.config/polybar/launch.sh
```sh
#!/bin/bash

killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

polybar example &
```

Le damos permisos de ejecucion
```sh
chmod +x ~/.config/polybar/launch.sh
```

Y añadimos a `~/.config/bspwm/bspwmrc`
```sh
echo "$HOME/.config/polybar/launch.sh" >> ~/.config/bspwm/bspwmrc
```

#### Cambiamos configuracion de la barra

En la parte izquierda dejamos los xworkspaces y xwindow
```sh
sed -i 's/^modules-left = [ A-Za-z0-9]*$/modules-left = xworkspaces xwindow/' ~/.config/polybar/config.ini
```

En la parte derecha dejamos sonido cpu memoria hora/fecha
```sh
sed -i 's/^modules-right = [ A-Za-z0-9]*$/modules-right = alsa cpu memory date/' ~/.config/polybar/config.ini
# Añadir a ver si va el modulo wlan
```


Le quitamos padding (si queremos) para hacer mas pequeña la barra
```sh
sed -i '/label-[a-z]*-padding = 2/ s/= 2/= 1/' ~/.config/polybar/config.ini
```


Con este podemos quitar el subrayado de los workspaces. No lo hago de momento
```sh
#sed -i '/underline/ s/^/;/' ~/.config/polybar/config
```

Podemos cambiar los indices por circulos a módulo bspwm (nerd font), mejor cambiamos el nombre de los escritorios (mas abajo)
```sh
#sed -i 's/label-active = %name%/label-active = /' ~/.config/polybar/config.ini
#sed -i 's/label-occupied = %name%/label-occupied = /' ~/.config/polybar/config.ini
#sed -i 's/label-urgent = %name%!/label-urgent = /' ~/.config/polybar/config.ini
#sed -i 's/label-empty = %name%/label-empty = /' ~/.config/polybar/config.ini
```

Cambiamos los nombres de los escritorios por iconos
```sh
sed -i 's/^bspc monitor -d .*$/bspc monitor -d   󰡨 󰷉 /' ~/.config/bspwm/bspwmrc
```

# Menu Rofi

Instalamos
```sh
sudo pacman -S rofi
mkdir -p ~/.config/rofi
```

Creamos le archivo ~/.config/rofi/config.rasi
```sh
configuration {
 modi: "window,drun,ssh,combi";
 font: "Hack Nerd Font 10";
 combi-modi: "window,drun,ssh";
}
@theme "solarized"
```

Cambiamos el menu
```sh
sed -i 's/dmenu_run/rofi -show combi/' ~/.config/sxhkd/sxhkdrc
```
