#!/bin/bash

SCRIPTPATH=$(dirname "$0")

sh ${SCRIPTPATH}/1-bspwm-install.sh
sh ${SCRIPTPATH}/2-picom-install.sh
sh ${SCRIPTPATH}/3-polybar-install.sh
sh ${SCRIPTPATH}/4-rofi-install.sh
sh ${SCRIPTPATH}/5-scrot-install.sh
