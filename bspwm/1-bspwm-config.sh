#!/bin/bash
#
# bspawn
#

sed -i "s/^PS1/#PS1/" ~/.bashrc
echo "PS1='\[\e[1;34m\]→ \[\e[0;34m\]\w \[\e[0m\]'" >> ~/.bashrc
echo '' >> ~/.bashrc
echo 'export XDG_CONFIG_HOME=$HOME/.config' >> ~/.bashrc

echo "exec bspwm" > ~/.xsession

mkdir -p ~/.config/{bspwm,sxhkd}
cp /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/
sed -i 's/urxvt/xterm/' ~/.config/sxhkd/sxhkdrc

# cp /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/
# sed -i 's/^bspc monitor -d .*$/bspc monitor -d   󰡨 󰷉 /' ~/.config/bspwm/bspwmrc
# sed -i 's/^bspc config border_width.*$/bspc config border_width         1/' ~/.config/bspwm/bspwmrc
# sed -i 's/^bspc config window_gap.*$/bspc config window_gap           6/' ~/.config/bspwm/bspwmrc
# sed -i "s/^bspc rule -a Gimp.*$/bspc rule -a Gimp desktop='\^5' state=floating follow=on/" ~/.config/bspwm/bspwmrc

# echo "bspc config focused_border_color '#4C0099'" >> ~/.config/bspwm/bspwmrc
# echo "bspc rule -a Inkscape desktop='^5' state=floating follow=on" >> ~/.config/bspwm/bspwmrc
# echo "bspc rule -a firefox desktop='^2'" >> ~/.config/bspwm/bspwmrc
# echo "bspc rule -a vlc state=floating" >> ~/.config/bspwm/bspwmrc

sudo cat > ~/.config/bspwm/bspwmrc <<- EOM
#! /bin/sh

pgrep -x sxhkd > /dev/null || sxhkd &

bspc monitor -d   󰡨 󰷉 

bspc config border_width         1
bspc config window_gap           6

bspc config split_ratio          0.52
bspc config borderless_monocle   true
bspc config gapless_monocle      true
bspc config focused_border_color '#4C0099'

bspc rule -a Gimp desktop='^5' state=floating follow=on
bspc rule -a Inkscape desktop='^5' state=floating follow=on
bspc rule -a Chromium desktop='^2'
bspc rule -a firefox desktop='^2' follow=on
bspc rule -a mplayer2 state=floating
bspc rule -a vlc state=floating
bspc rule -a Screenkey manage=off

EOM

chmod +x ~/.config/bspwm/bspwmrc

#
# keyboard
#
sudo cat > /tmp/00-keyboard.conf <<- EOM
Section "InputClass"
        Identifier "system-keyboard"
        MatchIsKeyboard "on"
        Option "XkbLayout" "es"
EndSection
EOM
sudo mv /tmp/00-keyboard.conf /etc/X11/xorg.conf.d/

#
# lightdm
#
sudo sed -i 's/^#greeter-session=.*$/greeter-session=lightdm-gtk-greeter/' /etc/lightdm/lightdm.conf
sudo sed -i 's/^#background=.*$/background=\/usr\/share\/wallpapers\/Arch-Wallpaper-1080-3.png/' /etc/lightdm/lightdm-gtk-greeter.conf
sudo sed -i 's/^#position=.*$/position=15% 50%,center/' /etc/lightdm/lightdm-gtk-greeter.conf
sudo systemctl enable lightdm.service

#
# custom
#
cat > ~/.Xresources <<- EOM
!! ColorScheme

! Generated with :
! XRDB2Xreources.py
!
*.foreground:  #e5e5e5
*.background:  #232322
*.cursorColor: #16afca
!
! Black
*.color0:      #212121
*.color8:      #424242
!
! Red
*.color1:      #b7141f
*.color9:      #e83b3f
!
! Green
*.color2:      #457b24
*.color10:     #7aba3a
!
! Yellow
*.color3:      #f6981e
*.color11:     #ffea2e
!
! Blue
*.color4:      #134eb2
*.color12:     #54a4f3
!
! Magenta
*.color5:      #560088
*.color13:     #aa4dbc
!
! Cyan
*.color6:      #0e717c
*.color14:     #26bbd1
!
! White
*.color7:      #efefef
*.color15:     #d9d9d9
!
! Bold, Italic, Underline
*.colorBD:     #b7141f
!*.colorIT:
!*.colorUL:

!! URxvt Appearance

URxvt.font: 			xft:Hack Nerd Font Mono:pixelsize=12:antialias=false
URxvt.boldFont: 		xft:Hack Nerd Font Mono:bold:pixelsize=12:antialias=false
!! URxvt.letterSpace:		-1

!! URxvt Config

URxvt.scrollBar:      		      false

!! xterm Appearance

xterm*faceName: xft:Hack Nerd Font Mono
xterm*faceSize: 12
xterm*loginshell: false
EOM

#
# fonts
#
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.1/Hack.zip
sudo unzip Hack.zip -d /usr/share/fonts/
rm Hack.zip
