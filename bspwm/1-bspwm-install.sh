#!/bin/bash

sudo pacman -S bspwm sxhkd xorg-server xorg-xinit xorg-xrandr --noconfirm

sudo pacman -S feh dmenu rxvt-unicode xterm curl wget git unzip --noconfirm

sudo pacman -S lightdm lightdm-gtk-greeter --noconfirm
